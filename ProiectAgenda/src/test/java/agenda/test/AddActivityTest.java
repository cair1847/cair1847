package agenda.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import agenda.model.base.Activity;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.interfaces.RepositoryActivity;

import org.junit.Before;
import org.junit.Test;

public class AddActivityTest {
	private Activity act;
	private RepositoryActivity rep;
	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryActivityMock();
	}
	
	@Test
	public void testCase1()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			act = new Activity("user1",
					df.parse("5/21/2018 10:30"),
					df.parse("5/21/2018 11:30"),
					null,
					"desc1");
			assertTrue(rep.addActivity(act));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertTrue(1 == rep.count());
	}
	
	@Test
	public void testCase2()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
			for (Activity a : rep.getActivities())
				rep.removeActivity(a);

			act = new Activity("user1",
					df.parse("5/21/2018 10:30"),
					df.parse("5/21/2018 11:30"),
					null,
					"desc1");
			assertTrue(rep.addActivity(act));

			act = new Activity("user1",
					df.parse("5/21/2018 10:30"),
					df.parse("5/21/2018 11:30"),
					null,
					"desc2");
			assertFalse(rep.addActivity(act));
		}
		catch(Exception e){}	
		int c = rep.count();
		assertTrue( c == 1);
		for (Activity a : rep.getActivities())
			rep.removeActivity(a);
	}
	
	@Test
	public void testCase3()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
			for (Activity a : rep.getActivities())
				rep.removeActivity(a);

			act = new Activity("user1",
					df.parse("5/22/2018 10:30"),
					df.parse("5/22/2018 11:30"),
					null,
					"desc1");
			assertTrue(rep.addActivity(act));

		}
		catch(Exception e){}	
		assertTrue( 1 == rep.count());
		rep.saveActivities();
		for (Activity a : rep.getActivities())
			rep.removeActivity(a);
	}

}
