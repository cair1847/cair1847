package agenda.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;
	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void testCase1()
	{
		try {
			con = new Contact("nume1", "adresa1", "+4071122334455");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}

	@Test
	public void testCase2()
	{
		try {
			con = new Contact("nume1,nume2", "adresa1,adresa2", "01122");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}

	@Test
	public void testCase3()
	{
		try {
			con = new Contact("nume1,nume2", "adresa2", "0823111111");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}

	@Test
	public void testCase4()
	{
		try {
			con = new Contact(",,,", "adresa1", "+4071122334455");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testCase5()
	{
		try {
			con = new Contact("nume1", "adresa1", "'f.e'f3r;;,,");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}

	}

	@Test
	public void testCase6()
	{
		try {
			con = new Contact("", "adresa1", "00");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}

	@Test
	public void testCase7()
	{
		try {
			con = new Contact("M", "adresa1", "0033");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}

	@Test
	public void testCase8()
	{
		try {
			con = new Contact("M", "", "0123455");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}

	@Test
	public void testCas9()
	{
		try {
			con = new Contact("nume1", "D", "0011");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}

	@Test
	public void testCas10() {
		try {
			con = new Contact("nume1", "adresa1", "f.e'f3r;;,,");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
	}

}
