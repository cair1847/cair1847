package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class IntegrationTestTD {

	private RepositoryActivity repAct;
	private RepositoryContact repCon;

	@Before
	public void setup() throws Exception {
		repCon = new RepositoryContactFile();
		repAct = new RepositoryActivityFile(repCon);

		for (Activity a : repAct.getActivities())
			repAct.removeActivity(a);
	}

	@Test
	public void testCase1() {
		int n = repCon.count();

		try {
			Contact c = new Contact("name", "address1", "+071122334455");
			repCon.addContact(c);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}

		assertTrue(n + 1 == repCon.count());
	}

	@Test
	public void testCase2() {
		boolean part1 = false, part2 = false;
		int n = repCon.count();

		try {
			Contact c = new Contact("name", "address1", "+071122334455");
			repCon.addContact(c);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}

		if (n + 1 == repCon.count())
			part1 = true;
		Activity act = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			act = new Activity("name1", df.parse("03/20/2013 12:00"),
					df.parse("03/20/2013 13:00"), null, "Lunch break");
			repAct.addActivity(act);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (repAct.getActivities().get(0).equals(act) && repAct.count() == 1)
			part2 = true;

		assertTrue(part1 && part2);
	}

	@Test
	public void testCase3() {
		boolean part1 = false, part2 = false;
		int n = repCon.count();

		try {
			Contact c = new Contact("name", "address1", "+071122334455");
			repCon.addContact(c);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}

		if (n + 1 == repCon.count())
			part1 = true;
		Activity act = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			act = new Activity("name1", df.parse("03/20/2013 12:00"),
					df.parse("03/20/2013 13:00"), null, "Lunch break");
			repAct.addActivity(act);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			repAct.addActivity((Activity) (Object) 5);
		} catch (Exception e) {
			part2 = true;
		}

		Calendar c = Calendar.getInstance();
		c.set(2013, 3 - 1, 20);

		List<Activity> result = repAct.activitiesOnDate("name1", c.getTime());
		assertTrue(result.size() == 1 && result.get(0).equals(act) && part1
				&& part2);
	}

}
